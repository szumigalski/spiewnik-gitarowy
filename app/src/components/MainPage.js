import React, { useState, useEffect } from 'react';
import {
  Header,
  Grid,
  Input,
  Button
} from 'semantic-ui-react'
import styled from 'styled-components'
import NewSongModal from './NewSongModal'
import NewHolyMassModal from './NewHolyMassModal'
import DrawChord from './DrawChord'
import axios from 'axios'
import _ from 'lodash'
import { NotificationManager} from 'react-notifications'
import { Link } from 'react-router-dom'

const MainPage = () => {
  const [isNewSongOpen, setNewSongOpen] = useState(false)
  const [songList, setSongList] = useState([])
  const [selectedSong, setSelectedSong] = useState({})
  const [holyMassList, setHolyMassList] = useState([])
  const [selectedHolyMass, setSelectedHolyMass] = useState({})
  const [isNewHolyMassOpen, setNewHolyMassOpen] = useState(false)
  const [holyMassFilter, setHolyMassFilter] = useState('')
  const [songFilter, setSongFilter] = useState('')
  //const customSearch = google.customsearch('v1')
  useEffect(() => {
    getSongs()
    getHolyMasses()
  }, [])

  useEffect(() => {
    !_.isEmpty(selectedSong) && setNewSongOpen(true)
  }, [selectedSong])

  useEffect(() => {
    !_.isEmpty(selectedHolyMass) && setNewHolyMassOpen(true)
  }, [selectedHolyMass])

  const getSongs = () => {
    axios.get('http://localhost:8080/api/songs')
      .then((res) => {
        setSongList(res.data.content.data)
      })
  }

  const getHolyMasses = () => {
    axios.get('http://localhost:8080/api/holyMass')
      .then((res) => {
        setHolyMassList(res.data.content.data)
      })
  }

  const deleteSong = (id) => {
    fetch(`http://localhost:8080/api/songs/${id}`, {
        method: 'DELETE',
        headers: {
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*'
        }
      }).then(() => {
      NotificationManager.success('Pomyślnie usunięto pieśń', 'Sukces', 3000);
      getSongs()
    })
    .catch((error) => {
      NotificationManager.error('Wystąpił błąd podczas usuwania pieśni', 'Błąd', 3000, () => {
        console.log(error)
      })
    })
}
  const editSong = (song) => {
    setSelectedSong({
      title: song.title ? song.title : '',
      body: song.body ? song.body : '',
      chords: song.chords ? song.chords : '',
      category: song.category ? song.category : '',
      _id: song._id ? song._id : ''
    })
  }

  const editHolyMass = (selectedHolyMass) => {
    setSelectedHolyMass({
      date: selectedHolyMass.date ? selectedHolyMass.date : '',
      piesnNaWejscie: selectedHolyMass.piesnNaWejscie ? selectedHolyMass.piesnNaWejscie : '',
      alleluja: selectedHolyMass.alleluja ? selectedHolyMass.alleluja : '',
      barankuBozy: selectedHolyMass.barankuBozy ? selectedHolyMass.barankuBozy : '',
      ofiarowanie: selectedHolyMass.ofiarowanie ? selectedHolyMass.ofiarowanie : '',
      piesnNaWyjscie: selectedHolyMass.piesnNaWyjscie ? selectedHolyMass.piesnNaWyjscie : '',
      _id: selectedHolyMass._id ? selectedHolyMass._id : ''
    })
  }

  const deleteHolyMass = (id) => {
    fetch(`http://localhost:8080/api/holyMass/${id}`, {
        method: 'DELETE',
        headers: {
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*'
        }
      }).then(() => {
        NotificationManager.success('Pomyślnie usunięto Mszę Świętą', 'Sukces', 3000);
        getHolyMasses()
      })
      .catch((error) => {
          NotificationManager.error('Wystąpił błąd podczas usuwania Mszy Świętej', 'Błąd', 3000, () => {
          console.log(error)
        })
      })
}

  return (
    <Container>
      <Header as='h1'>Śpiewnik gitarowy</Header>
      <Grid style={{ height: 'calc(100% - 20px)'}}>
        <ContainerColumn mobile={16} tablet={8} computer={8}>
          <Input
            placeholder='Szukaj Mszy...'
            value={holyMassFilter}
            onChange={({target : {value}}) => setHolyMassFilter(value)}
            style={{ border: '1px solid grey', borderRadius: 5, color: 'grey' }}
            />
            <List>
              {holyMassList.length ? holyMassList.map(holyMass =>
              holyMass.date.toLowerCase().includes(holyMassFilter.toLowerCase()) && <ListItem>
                <Link to={`/holyMass/${holyMass._id}`} style={{color: 'black', width: '100%', height: '100%'}}>
                  {holyMass.date}
                </Link>
                <div style={{display: 'flex'}}>
                  <Button icon='edit' color='orange' onClick={() => editHolyMass(holyMass)} />
                  <Button icon='x' color='red' onClick={() => deleteHolyMass(holyMass._id)} />
                </div>
              </ListItem>
              ) : null}
            </List>
          <Button positive onClick={() => setNewHolyMassOpen(true)}>Dodaj Mszę</Button>
        </ContainerColumn>
        <ContainerColumn mobile={16} tablet={8} computer={8}>
          <Input
            placeholder='Szukaj Pieśni...'
            value={songFilter}
            onChange={({target : {value}}) => setSongFilter(value)}
            style={{ border: '1px solid grey', borderRadius: 5, color: 'grey' }}
            />
            <List>
              {songList.length ? songList.map(song =>
                song.title.toLowerCase().includes(songFilter.toLowerCase()) && <ListItem>
                  <Link to={`/song/${song._id}`} style={{color: 'black', width: '100%', height: '100%'}}>
                  {song.title}
                  </Link>
                  <div style={{display: 'flex'}}>
                    <Button icon='edit' color='orange' onClick={() => editSong(song)} />
                    <Button icon='x' color='red' onClick={() => deleteSong(song._id)} />
                  </div>
                </ListItem>
              ) : null}
            </List>
          <Button positive onClick={() => setNewSongOpen(true)}>Dodaj Pieśń</Button>
        </ContainerColumn>
      </Grid>
      <NewSongModal
        isOpen={isNewSongOpen}
        handleOpen={setNewSongOpen}
        getSongs={getSongs}
        song={selectedSong}
        setSelectedSong={setSelectedSong}
        />
      <NewHolyMassModal
        isOpen={isNewHolyMassOpen}
        handleOpen={setNewHolyMassOpen}
        songList={songList}
        getHolyMasses={getHolyMasses}
        propsholyMass={selectedHolyMass}
        setSelectedHolyMass={setSelectedHolyMass}
        />
    </Container>
  );
}
const Container = styled.div`
  margin: auto;
  max-width: 1000px;
  padding: 20px;
  height: 100vh;
  background-color: rgba(237, 237, 237, .5);
`
const ContainerColumn = styled(Grid.Column)`
  display: flex!important;
  flex-direction: column!important;
  height: 100%;
  justify-content: space-between;
`
const ListItem = styled.div`
  width: 100%;
  background-color: #e8e8e3;
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 10px 20px;
  margin-bottom: 15px;
  color: black;
  &:hover {
    color: black;
  }
`
const List = styled.div`
  height: 100%;
  overflow: auto;
  margin-top: 5px;
  margin-bottom: 5px;
`
export default MainPage
