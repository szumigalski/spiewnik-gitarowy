import React from 'react'
import styled from 'styled-components'

const drawChord = ({ chord, fret }) => {
    const generateFretNumbers = (number) => {
        return(
            <div style={{display: 'flex'}}>
                <div style={{ width: 25, marginLeft: 11}}>{number}</div>
                <div style={{ width: 25}}>{number + 1}</div>
                <div style={{ width: 25}}>{number + 2}</div>
                <div style={{ width: 25}}>{number + 3}</div>
                <div style={{ width: 25}}>{number + 4}</div>
            </div>
        )
    }
    return(
        <div>
            <Chord>
                <Horizontal top={1} />
                <Horizontal top={2} />
                <Horizontal top={3} />
                <Horizontal top={4} />
                <Horizontal top={5} />
                <Horizontal top={6} />
                <Vertical left={1} />
                <Vertical left={2} />
                <Vertical left={3} />
                <Vertical left={4} />
                <Circle top={20} left={50}>1</Circle>
            </Chord>
            {generateFretNumbers(fret)}
        </div>
        )
}
const Chord = styled.div`
    width: 125px;
    height: 75px;
    position: relative;
    border: .5px solid black;
    background-color: white;
`
const Horizontal = styled.div`
    position: absolute;
    width: 125px;
    height: 1px;
    border: .5px solid black;
    left: 0;
    top: ${({top}) => `${top*10}px`};
`
const Vertical = styled.div`
    position: absolute;
    width: .5px;
    height: 75px;
    border: .5px solid black;
    left: ${({left}) => `${left*25}px`};
    top: -1px;
`
const Circle = styled.div`
    font-size: 8px;
    width: 12px;
    height: 12px;
    border-radius: 50%;
    border: .5px solid black;
    top: ${({top}) => `${top}px`};
    left: ${({left}) => `${left}px`};
    padding-left: 3px;
    padding-bottom: 15px;
`
export default drawChord