import React, { useState, useEffect } from 'react'
import {
    Modal,
    Input,
    Button,
    TransitionablePortal,
    Dropdown,
    TextArea,
    Header,
    Grid
} from 'semantic-ui-react'
import _ from 'lodash'
import moment from 'moment'
import {NotificationManager} from 'react-notifications'

const NewHolyMassModal = ({ isOpen, handleOpen, getHolyMasses, propsholyMass, songList, setSelectedHolyMass }) => {

    const [holyMass, setHolyMass] = useState({
        date: '',
        piesnNaWejscie: '',
        alleluja: '',
        barankuBozy: '',
        ofiarowanie: '',
        piesnNaWyjscie: ''
    })
    const [songOptions, setSongOptions] = useState([])

    useEffect(() => {
        let localSongs = []
        songList.length && songList.map(song => {
            localSongs.push({
                id: song._id,
                value: song._id,
                text: song.title
            })
        })
        setSongOptions(localSongs)
    }, [songList])

    useEffect(() => {
        var d = new Date()
        propsholyMass && setHolyMass({
            date: propsholyMass.date ? propsholyMass.date : getSunday(),
            piesnNaWejscie: propsholyMass.piesnNaWejscie ? propsholyMass.piesnNaWejscie : '',
            alleluja: propsholyMass.alleluja ? propsholyMass.alleluja : '',
            barankuBozy: propsholyMass.barankuBozy ? propsholyMass.barankuBozy : '',
            ofiarowanie: propsholyMass.ofiarowanie ? propsholyMass.ofiarowanie : '',
            piesnNaWyjscie: propsholyMass.piesnNaWyjscie ? propsholyMass.piesnNaWyjscie : '',
            _id: propsholyMass._id ? propsholyMass._id : ''
        })
    }, [propsholyMass])

    const getSunday = () => {
        var d = new Date()
        let x = new Date(d.getFullYear(), d.getMonth(), d.getDate() - d.getDay() + 7)
        return moment(x).format('DD-MM-YYYY')
    }

    const addHolyMass = () => {
        fetch('http://localhost:8080/api/holyMass', {
            method: 'POST',
            body: JSON.stringify({
                "date": holyMass.date,
                "piesnNaWejscie": holyMass.piesnNaWejscie,
                "alleluja": holyMass.alleluja,
                "barankuBozy": holyMass.barankuBozy,
                "ofiarowanie": holyMass.ofiarowanie,
                "piesnNaWyjscie": holyMass.piesnNaWyjscie
            }),
            headers: {
                'Content-Type': 'application/json',
                'Access-Control-Allow-Origin': '*'
            }
          }).then(() => {
            NotificationManager.success('Pomyślnie utworzono Mszę Świętą', 'Sukces', 3000)
            getHolyMasses()
            clearHolyMass()
            handleOpen(false)
          })
          .catch(error => {
            NotificationManager.error('Wystąpił błąd podczas dodawania Mszy Świętej', 'Błąd', 3000, () => {
            console.log(error)
            })
          })
    }

    const updateHolyMass = () => {
        fetch(`http://localhost:8080/api/holyMass/${holyMass._id}`, {
            method: 'PUT',
            body: JSON.stringify({
                "date": holyMass.date,
                "piesnNaWejscie": holyMass.piesnNaWejscie,
                "alleluja": holyMass.alleluja,
                "barankuBozy": holyMass.barankuBozy,
                "ofiarowanie": holyMass.ofiarowanie,
                "piesnNaWyjscie": holyMass.piesnNaWyjscie
            }), // data can be `string` or {object}!
            headers: {
                'Content-Type': 'application/json',
                'Access-Control-Allow-Origin': '*'
            }
          }).then(() => {
            NotificationManager.success('Pomyślnie zapisano Mszę Świętą', 'Sukces', 3000)
            getHolyMasses()
            clearHolyMass()
            handleOpen(false)
          }).catch(error => {
            NotificationManager.error('Wystąpił błąd podczas zapisywania Mszy Świętej', 'Błąd', 3000, () => {
            console.log(error)
            })
          })
    }

    const clearHolyMass = () => {
        setHolyMass({
            date: '',
            piesnNaWejscie: '',
            alleluja: '',
            barankuBozy: '',
            ofiarowanie: '',
            piesnNaWyjscie: ''
        })
        setSelectedHolyMass({})
    }

    const handleChange = (field, value) => {
        let change = { ...holyMass }
        change[field] = value
        setHolyMass(change)
    }

    const formFields = [
        ['Pieśń na wejście', 'piesnNaWejscie'],
        ['Alleluja', 'alleluja'],
        ['Baranku Boży', 'barankuBozy'],
        ['Ofiarowanie', 'ofiarowanie'],
        ['Pieśń na wyjście', 'piesnNaWyjscie']
    ]

    return(
        <TransitionablePortal open={isOpen}>
            <Modal open>
                <Modal.Header>
                    Data: <Input
                        placeholder='Dodaj datę Mszy...'
                        value={holyMass.date}
                        style={{ border: '1px solid grey', borderRadius: 5, color: 'grey' }}
                        onChange={({target : {value}}) => handleChange('date', value)}
                        />
                </Modal.Header>
                <Modal.Content style={{ display: 'flex', flexDirection: 'column', height: '100%' }}>
                {formFields.map((field, i) => <div key={i} style={{ display: 'flex', marginBottom: 10, alignItems: 'center' }}>
                        <Header as='h3' style={{width: 200, margin: 0}}>{field[0]}</Header>
                        <Dropdown
                            value={holyMass[field[1]]}
                            placeholder={`Wybierz ${field[0].toLowerCase()}...`}
                            style={{ border: '1px solid grey', borderRadius: 5, color: 'grey' }}
                            onChange={(event, {value}) => handleChange(field[1], value)}
                            fluid
                            options={songOptions}
                            selection
                            />
                </div>)}
                </Modal.Content>
                <Modal.Actions>
                    <Button color='red' onClick={() => {
                        handleOpen(false)
                        clearHolyMass()
                        }}>
                        Cofnij
                    </Button>
                    <Button color='green' onClick={() => !holyMass._id  ? addHolyMass() : updateHolyMass()}>
                        Zapisz
                    </Button>
                </Modal.Actions>
            </Modal>
        </TransitionablePortal>
    )
}

export default NewHolyMassModal