import React, { useState, useEffect } from 'react'
import { withRouter } from "react-router"
import axios from 'axios'
import styled from 'styled-components'
import {
  Header,
  Dropdown,
  Button
} from 'semantic-ui-react'
import { Link } from 'react-router-dom'

const HolyMass = ({ match }) => {
    const [holyMass, setHolyMass] = useState({})
    const [songOptions, setSongOptions] = useState([])
    const [songList, setSongList] = useState([])

    useEffect(() => {
        getHolyMass()
        axios.get('http://localhost:8080/api/songs')
        .then((res) => {
            setSongList([...res.data.content.data])
        })
    }, [])

    useEffect(() => {
        let localSongs = []
        console.log(holyMass)
        songList.length && songList.map(song => {
            localSongs.push({
                id: song._id,
                value: song._id,
                text: song.title
            })
        })
        setSongOptions(localSongs)
    }, [songList])

    const handleChange = (field, value) => {
        let change = { ...holyMass }
        change[field] = value
        setHolyMass(change)
    }

    const getHolyMass = () => {
        axios.get(`http://localhost:8080/api/holyMass/${match.params.id}`)
          .then((res) => {
            setHolyMass(res.data)
          })
      }
    const formFields = [
        ['Pieśń na wejście', 'piesnNaWejscie'],
        ['Alleluja', 'alleluja'],
        ['Baranku Boży', 'barankuBozy'],
        ['Ofiarowanie', 'ofiarowanie'],
        ['Pieśń na wyjście', 'piesnNaWyjscie']
    ]
    return(
     <Container>
        <Header as='h2'>{holyMass.date}</Header>
        <Link to='/'>Back</Link>
        {formFields.map((field, i) => <div key={i} style={{ display: 'flex', marginBottom: 10, alignItems: 'center' }}>
                        <Header as='h3' style={{width: 200, margin: 0}}>{field[0]}</Header>
                        <Dropdown
                            value={holyMass[field[1]]}
                            placeholder={`Wybierz ${field[0].toLowerCase()}...`}
                            style={{ border: '1px solid grey', borderRadius: 5, color: 'grey' }}
                            onChange={(event, {value}) => handleChange(field[1], value)}
                            fluid
                            options={songOptions}
                            selection
                            />
                        <Link to={`/song/${holyMass[field[1]]}`}><Button
                            positive
                            icon='arrow right'
                            />
                            </Link>
                </div>)}
     </Container>
    )
}
const Container = styled.div`
  margin: auto;
  max-width: 1000px;
  padding: 20px;
  height: 100vh;
  background-color: #f2f2f0;
  box-sizing: border-box;
`
export default withRouter(HolyMass)