import React, { useState, useEffect } from 'react'
import {
    Modal,
    Input,
    Button,
    TransitionablePortal,
    Dropdown,
    TextArea,
    Header,
    Grid,
    Tab
} from 'semantic-ui-react'
import _ from 'lodash'
import {NotificationManager} from 'react-notifications'

const NewSongModal = ({ isOpen, handleOpen, getSongs, song, setSelectedSong }) => {

    const [title, setTitle] = useState('')
    const [body, setBody] = useState('')
    const [chords, setChords] = useState(['', '', ''])
    const [category, setCategory] = useState('')
    const [rowCounter, setRowCounter] = useState(0)
    const [lineNumber, setLineNumber] = useState(['1:'])

    useEffect(() => {
        let localChords = [...chords]
        for(let j=0; j<3; j+=1) {
            let counter = rowCounter > lineCounter(chords[j]) ? rowCounter - lineCounter(chords[j]) : 0
            let newLines = ''
            for(let i = 0; i<counter; i += 1 ) {
                newLines += `\n`
            }
            localChords[j] += newLines
        }
        setChords(localChords)
    }, [rowCounter])

    const lineCountString = (number) => {
        let string = []
        for(let i=1; i<=number+1; i+=1) {
            string.push(i + ':')
        }
        setLineNumber(string)
    }

    useEffect(() => {
        song && setTitle(song.title ? song.title : '')
        song && setBody(song.body ? song.body : '')
        song && setChords(song.chords ? song.chords : ['', '', ''])
        song && setCategory(song.category ? song.category : '')
    }, [song])

    const addSong = () => {
        let promise = fetch('http://localhost:8080/api/songs', {
            method: 'POST', // or 'PUT'
            body: JSON.stringify({
                "title": title,
                "body": body,
                "chords": chords,
                "category": category
            }), // data can be `string` or {object}!
            headers: {
                'Content-Type': 'application/json',
                'Access-Control-Allow-Origin': '*'
            }
          }).then(() => {
            NotificationManager.success('Pomyślnie utworzono pieśń', 'Sukces', 3000)
            getSongs()
            clearSong()
            handleOpen(false)
          })
          .catch(error=>{
            NotificationManager.error('Wystąpił błąd podczas dodawania pieśni', 'Błąd', 3000, () => {
                console.log(error)
                })
          })
    }

    const updateSong = () => {
        fetch(`http://localhost:8080/api/songs/${song._id}`, {
            method: 'PUT', // or 'PUT'
            body: JSON.stringify({
                "title": title,
                "body": body,
                "chords": chords,
                "category": category
            }), // data can be `string` or {object}!
            headers: {
                'Content-Type': 'application/json',
                'Access-Control-Allow-Origin': '*'
            }
          }).then(() => {
            NotificationManager.success('Pomyślnie zapisano pieśń', 'Sukces', 3000)
            getSongs()
            clearSong()
            handleOpen(false)
          }).catch(error => {
            NotificationManager.error('Wystąpił błąd podczas zapisywania pieśni', 'Błąd', 3000, () => {
            console.log(error)
            })
          })
    }

    const clearSong = () => {
        setTitle('')
        setBody('')
        setChords(['', '', ''])
        setCategory('')
        setSelectedSong()
    }

    const handleChangeBody = (value) => {
        value && setRowCounter(lineCounter(value))
        value && lineCountString(lineCounter(value))
        setBody(value)
    }

    const lineCounter = (value) => {
        let lines = value ? value.split(/\r|\r\n|\n/) : ''
        return lines.length
    }
    const handleChords = (number, value) => {
        let localChords = [...chords]
        localChords[number] = value
        setChords(localChords)
    }
    const categories = [
        {id: 1, value: 'alleluja', text: 'Alleluja'},
        {id: 2, value: 'barankuBozy', text: 'Baranku Boży'},
        {id: 3, value: 'inne', text: 'Inne'}
    ]
    const panes = [
        {
          menuItem: 'Tab 1',
          render: () => <TextArea
          value={chords[0]}
          rows={rowCounter +1}
          style={{height: '100%', width: '100%'}}
          onChange={({target : {value}}) => handleChords(0, value)}
          />,
        },
        {
          menuItem: 'Tab 2',
          render: () => <TextArea
          value={chords[1]}
          rows={rowCounter +1}
          style={{height: '100%', width: '100%'}}
          onChange={({target : {value}}) => handleChords(1, value)}
          />,
        },
        {
          menuItem: 'Tab 3',
          render: () => <TextArea
          value={chords[2]}
          rows={rowCounter +1}
          style={{height: '100%', width: '100%'}}
          onChange={({target : {value}}) => handleChords(2, value)}
          />,
        },
      ]
    return(
        <TransitionablePortal open={isOpen}>
            <Modal open style={{height: 'calc(90vh - 220px)'}}>
                <Modal.Header>
                    Tytuł: <Input
                        placeholder='Dodaj tytuł...'
                        value={title}
                        style={{ border: '1px solid grey', borderRadius: 5, color: 'grey' }}
                        onChange={({target : {value}}) => setTitle(value)}
                        />
                </Modal.Header>
                <Modal.Content style={{ display: 'flex', flexDirection: 'column', height: '100%' }}>
                    <Grid style={{overflow: 'auto', height: '100%'}}>
                        <Grid.Column mobile={16} tablet={12} computer={12} style={{width: '100%'}}>
                            <Header as='h3'>Tekst Piosenki:</Header>
                            <div style={{ display: 'flex', height: '100%'}}>
                            <div style={{backgroundColor: '#eee', overflow: 'auto', margin: '0 3px', fontSize: '100%'}}>{lineNumber.length &&
                            lineNumber.map(line =>
                            <div>{line}</div>)}</div>
                            <TextArea
                                value={body}
                                style={{height: '100%', width: '100%'}}
                                onChange={({target : {value}}) => handleChangeBody(value)}
                                />
                            </div>
                        </Grid.Column>
                        <Grid.Column mobile={16} tablet={4} computer={4}>
                            <Header as='h3'>Akordy:</Header>
                            <Tab menu={{ secondary: true, pointing: true }} panes={panes} />
                        </Grid.Column>
                    </Grid>
                    <Header as='h3'>Kategoria:</Header>
                    <Dropdown
                        value={category}
                        placeholder='Wybierz kategorię...'
                        style={{ border: '1px solid grey', borderRadius: 5, color: 'grey' }}
                        onChange={(event, {value}) => setCategory(value)}
                        options={categories}
                        fluid
                        selection
                        />
                </Modal.Content>
                <Modal.Actions>
                    <Button color='red' onClick={() => {
                        handleOpen(false)
                        clearSong()
                        }}>
                        Cofnij
                    </Button>
                    <Button color='green' onClick={() => song && song._id && song._id !== '' ? updateSong() : addSong()}>
                        Zapisz
                    </Button>
                </Modal.Actions>
            </Modal>
        </TransitionablePortal>
    )
}

export default NewSongModal