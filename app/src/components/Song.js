import React, { useState, useEffect } from 'react'
import { withRouter } from "react-router"
import axios from 'axios'
import styled from 'styled-components'
import {
  Header,
  Button,
  Tab
} from 'semantic-ui-react'
import { Link } from 'react-router-dom'

const Song = ({ match }) => {
    const [song, setSong] = useState({})
    const [transpone, setTranspone] = useState(0)

    useEffect(() => {
        getSong()
    }, [])

    const getSong = () => {
        axios.get(`http://localhost:8080/api/songs/${match.params.id}`)
          .then((res) => {
            res.data.body && (res.data.body = res.data.body.split(/\r|\r\n|\n/))
            res.data.chords && (res.data.chords.map((line, i) =>
              res.data.chords[i] = line.split(/\r|\r\n|\n/))
            )
            setSong(res.data)
          })
      }
      const panes = [
        {
          menuItem: 'Tab 1',
          render: () => song.chords && song.chords[0] && song.chords[0].map(chord =>
            <div style={{ fontSize: 18, marginBottom: 4}}>{chord}</div>
          ),
        },
        {
          menuItem: 'Tab 2',
          render: () => song.chords && song.chords[1] && song.chords[1].map(chord =>
            <div style={{ fontSize: 18, marginBottom: 4}}>{chord}</div>
          ),
        },
        {
          menuItem: 'Tab 3',
          render: () => song.chords && song.chords[2] && song.chords[2].map(chord =>
            <div style={{ fontSize: 18, marginBottom: 4}}>{chord}</div>
          ),
        },
      ]
    return(
     <Container>
        <Header as='h2'>{song.title}</Header>
        <div style={{display: 'flex', justifyContent: 'space-between', alignItems: 'center'}}>
        <Link to='/'>Back</Link>
        <div style={{ display: 'flex', alignItems: 'center', fontSize: 20 }}>
        <Button icon='arrow down' onClick={() => setTranspone(transpone - 1)} />
        {transpone}
        <Button icon='arrow up' onClick={() => setTranspone(transpone + 1)} />
        </div>
        </div>
        <div style={{display: 'flex'}}>
          <SongBody>
            {song.body && song.body.map(line =>
              <div style={{ fontSize: 18, marginBottom: 4}}>{line}</div>
            )}
          </SongBody>
          <SongChords>
            <Tab menu={{ secondary: true, pointing: true }} panes={panes} />
          </SongChords>
        </div>
     </Container>
    )
}
const Container = styled.div`
  margin: auto;
  max-width: 1000px;
  padding: 20px;
  height: 100vh;
  background-color: #f2f2f0;
  box-sizing: border-box;
`
const SongBody = styled.div`
  background-color: white;
  padding: 20px;
  width: 70%;
  padding-top: 80px;
`
const SongChords = styled.div`
  background-color: white;
  padding: 20px;
  width: 30%;
`
export default withRouter(Song)