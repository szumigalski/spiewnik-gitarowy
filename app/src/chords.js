export default chords = [
    {
        name: 'C',
        first: 1,
        table: [
                [[2, 1]],
                [[3, 2]],
                [[5, 4]],
                [[6, 3]],
                []
        ]
    },
    {
        name: 'D',
        first: 1,
        table: [
                [],
                [[,1], [,3]],
                [[,2]],
                [],
                []
        ]
    },
    {
        name: 'E',
        first: 1,
        table: [
                [[,3]],
                [[,4], [,5]],
                [],
                [],
                []
        ]
    },
    {
        name: 'F',
        first: 1,
        table: [
                [[0]],
                [],
                [],
                [],
                []
        ]
    },
    {
        name: 'G',
        first: 1,
        table: [
                [],
                [],
                [[,1], [,2], [,6]],
                [[,5]],
                []
        ]
    },
    {
        name: 'A',
        first: 1,
        table: [
                [],
                [[,3], [,4], [,5]],
                [],
                [],
                []
        ]
    },
    {
        name: 'H',
        first: 1,
        table: [
                [],
                [[0]],
                [],
                [],
                []
        ]
    }
]