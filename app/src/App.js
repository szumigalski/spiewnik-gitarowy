import React from 'react';
import _ from 'lodash'
import 'react-notifications/lib/notifications.css'
import {NotificationContainer} from 'react-notifications'
import MainPage from './components/MainPage'
import Song from './components/Song'
import HolyMass from './components/HolyMass'
import { createGlobalStyle } from 'styled-components'
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

const App = () => {
  return(<Router>
        <Switch>
          <Route path="/song/:id">
            <Song />
          </Route>
          <Route path="/holyMass/:id">
            <HolyMass />
          </Route>
          <Route path="/">
            <MainPage />
          </Route>
        </Switch>
      <GlobalStyle/>
      <NotificationContainer/>
    </Router>
  )
}
const GlobalStyle = createGlobalStyle`
  * {
    /*
    @import url('https://fonts.googleapis.com/css?family=Montserrat&display=swap');
    font-family: 'Montserrat', sans-serif!important;*/
  }
`
export default App;
