import bodyParser from 'body-parser'
import mongoose from 'mongoose'
import express from 'express'
import route from './routes'

let devPort = 8080

let port = process.env.PORT || devPort

const app = express()

let mongoPathDev = `mongodb://admin:Spiewnik1@ds039457.mlab.com:39457/spiewnik`

mongoose.Promise = global.Promise
mongoose.connect(mongoPathDev, { useNewUrlParser: true })
mongoose.set('useCreateIndex', true)

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

app.use(route.allowCrossDomain)
app.use('/api', route.router)
app.get('/test', function (req, res) {
  res.send('test')
})
app.use((req, res, next) => {
  const error = new Error('Not found')
  error.status = 404
  next(error)
})

app.use((error, req, res, next) => {
  res.status(error.status || 500)
  res.json({
    error: {
      message: error.message
    }
  })
})

app.listen(port)

console.log('RESTful API server started on: ' + port)
