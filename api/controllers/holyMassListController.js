import HolyMass from '../models/holyMassListModel'
import _ from 'lodash'

exports.list_all_holy_masses = function(req, res) {
  let totalElements = 0
  HolyMass.count({}, (err, allElementsLength) =>  {
    totalElements = allElementsLength
  })
  HolyMass.find({})
    .exec((err, holyMass) => {
    if (err)
      res.send(err)
      res.json({
      content: {
        data: holyMass,
        totalElements: totalElements,
        pageElements: holyMass.length
      }
    })
  })
}

exports.create_a_holy_mass = function(req, res) {
  let new_holy_mass = new HolyMass(req.body)
  new_holy_mass.save(function(err, holy_mass) {
    if (err)
      res.send(err)
    res.json(holy_mass)
  })
}

exports.read_a_holy_mass = function(req, res) {
    HolyMass.findById(req.params.holyMassId, function(err, holyMass) {
    if (err)
      res.send(err)
    res.json(holyMass)
  })
}

exports.update_a_holy_mass = function(req, res) {
    HolyMass.findOneAndUpdate({_id:req.params.holyMassId}, req.body, {new: true}, function(err, holyMass) {
    if (err)
      res.send(err)
    res.json(holyMass)
  })
}

exports.delete_a_holy_mass = function(req, res) {
    HolyMass.remove({
    _id: req.params.holyMassId
  }, function(err, holyMass) {
    if (err)
      res.send(err)
    res.json({ message: 'Holy Mass successfully deleted' })
  })
}
