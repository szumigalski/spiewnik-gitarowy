import mongoose from 'mongoose'

const songSchema = new mongoose.Schema({
    title: {
        type: String,
        required: true,
        unique: true
    },
    body: {
        type: String,
        required: true,
        unique: false
    },
    chords: {
        type: Array,
        required: false,
        unique: false
    },
    youtube: {
        type: String,
        required: false,
        unique: false
    },
    category: {
        type: String,
        required: false,
        unique: false
    }
})

module.exports = mongoose.model('Song', songSchema)