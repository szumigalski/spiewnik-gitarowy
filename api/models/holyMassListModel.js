import mongoose from 'mongoose'

const holyMassSchema = new mongoose.Schema({
    date: {
        type: String,
        required: true,
        unique: true
    },
    piesnNaWejscie: {
        type: String,
        required: false,
        unique: false
    },
    alleluja: {
        type: String,
        required: false,
        unique: false
    },
    ofiarowanie: {
        type: String,
        required: false,
        unique: false
    },
    barankuBozy: {
        type: String,
        required: false,
        unique: false
    },
    piesnNaWyjscie: {
        type: String,
        required: false,
        unique: false
    }
})

module.exports = mongoose.model('HolyMass', holyMassSchema)