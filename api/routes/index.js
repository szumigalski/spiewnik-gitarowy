import express from 'express'
import songRouter from './songListRoutes'
import holyMassRouter from './holyMassListRoutes'

const router = express.Router()

const allowCrossDomain = (req, res, next) => {
  res.header('Access-Control-Allow-Credentials', true)
  res.header('Access-Control-Allow-Origin', '*')
  res.header('Access-Control-Allow-Methods', 'GET, PUT, POST, PATCH, DELETE, OPTIONS')
  res.header('Access-Control-Allow-Headers', '*')
  next()
}
router.use('/songs', songRouter)
router.use('/holyMass', holyMassRouter)

export default {
  allowCrossDomain,
  router
}
