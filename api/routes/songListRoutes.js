import express from 'express'
import songController from '../controllers/songListController'

const router = express.Router()

router.get('/', songController.list_all_songs)
  .get('/:songId', songController.read_a_song)
	.post('/', songController.create_a_song)
	.delete('/:songId', songController.delete_a_song)
	.put('/:songId', songController.update_a_song)

export default router