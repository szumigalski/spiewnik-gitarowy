import express from 'express'
import holyMassController from '../controllers/holyMassListController'

const router = express.Router()

router.get('/', holyMassController.list_all_holy_masses)
  .get('/:holyMassId', holyMassController.read_a_holy_mass)
	.post('/', holyMassController.create_a_holy_mass)
	.delete('/:holyMassId', holyMassController.delete_a_holy_mass)
	.put('/:holyMassId', holyMassController.update_a_holy_mass)

export default router